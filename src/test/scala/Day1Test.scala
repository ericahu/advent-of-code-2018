import org.scalatest.FunSuite

class Day1Test extends FunSuite {
  val testInput1 = "+1, -2, +3, +1".split(", ").toList
 //  val testInput2 = "+1, +1, +1".split(", ").toList
 //  val testInput3 = "+1, +1, -2".split(", ").toList
 //  val testInput4 = "-1, -2, -3".split(", ").toList
 //
  test("Day1.Problem1") {
    assert(Day1.solve1(testInput1) === 3)
 //    assert(Day1.solve1(testInput2) === 3)
 //    assert(Day1.solve1(testInput3) === 0)
 //    assert(Day1.solve1(testInput4) === -6)
  }
 //  test("Day1.Problem1_Actual") {
 //    assert(Day1.solve1() === 595)
 //  }
 //
 //  val testInput5 = "+1, -1".split(", ").toList
 //  val testInput6 = "+3, +3, +4, -2, -4".split(", ").toList
 //  val testInput7 = "-6, +3, +8, +5, -6".split(", ").toList
 //  val testInput8 = "+7, +7, -2, -7, -4".split(", ").toList
 //
 // test("Day1.Problem2") {
 //   assert(Day1.solve2(testInput1) === 2)
 //   assert(Day1.solve2(testInput5) === 0)
 //   assert(Day1.solve2(testInput6) === 10)
 //   assert(Day1.solve2(testInput7) === 5)
 //   assert(Day1.solve2(testInput8) === 14)
 // }
 /* This test takes takes 2 min to run */
 // test("Day1.Problem2_Actual") {
 //   assert(Day1.solve2() === 80598)
 // }r
}
