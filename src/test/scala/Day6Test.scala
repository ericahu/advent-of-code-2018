import org.scalatest.FunSuite

class Day6Test extends FunSuite {
  val testInput = List("1, 1", "1, 6", "8, 3", "3, 4", "5, 5", "8, 9")

  test("Day6.Problem1") {
    assert(Day6.solve1(testInput) === 17)
  }
  test("Day6.Problem1_Actual") {
    assert(Day6.solve1() === 3620)
  }
  test("Day6.Problem2") {
    assert(Day6.solve2(testInput, 32) === 16)
  }
  test("Day6.Problem2_Actual") {
    assert(Day6.solve2() === 39930)
  }
}
