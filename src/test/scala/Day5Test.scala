import org.scalatest.FunSuite

class Day5Test extends FunSuite {
  val testInput = "dabAcCaCBAcCcaDA"
  test("Day5.Problem1") {
    assert(Day5.solve1(testInput) === 10)
  }
  test("Day5.Problem2") {
    assert(Day5.solve2(testInput) === 4)
  }
  // test("Day5.Problem1_Actual") {
  //   assert(Day5.solve1() === 10496)
  // }
  // test("Day5.Problem2_Actual") {
  //   assert(Day5.solve2() === 5774)
  // }
}
