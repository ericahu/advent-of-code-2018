import org.scalatest.FunSuite

class Day3Test extends FunSuite {
  val testInput = """#1 @ 1,3: 4x4
                    |#2 @ 3,1: 4x4
                    |#3 @ 5,5: 2x2"""
                    .stripMargin
                    .split("\n")
                    .toList

  test("Day3.Problem1") {
    assert(Day3.solve1(testInput) === 4)
  }
  test("Day3.Problem1_Actual") {
    assert(Day3.solve1() === 111630)
  }
  test("Day3.Problem2") {
    assert(Day3.solve2(testInput) === 3)
  }
  test("Day3.Problem2_Actual") {
    assert(Day3.solve2() === 724)
  }
}
