import org.scalatest.FunSuite

class Day2Test extends FunSuite {
  val testInput1 = List("abcdef", "bababc", "abbcde", "abcccd", "aabcdd", "abcdee", "ababab")
  val testInput2 = List("abcde", "fghij", "klmno", "pqrst", "fguij", "axcye", "wvxyz")

  test("Day2.Problem1") {
    assert(Day2.solve1(testInput1) === 12)
  }
  test("Day2.Problem1_Actual") {
    assert(Day2.solve1() === 7134)
  }
  test("Day2.Problem2") {
    assert(Day2.solve2(testInput2) === "fgij")
  }
  test("Day2.Problem2_Actual") {
    assert(Day2.solve2() === "kbqwtcvzhmhpoelrnaxydifyb")
  }
}
