import org.scalatest.FunSuite

class Day7Test extends FunSuite {
  val testInput = List("Step C must be finished before step A can begin.",
                       "Step C must be finished before step F can begin.",
                       "Step A must be finished before step B can begin.",
                       "Step A must be finished before step D can begin.",
                       "Step B must be finished before step E can begin.",
                       "Step D must be finished before step E can begin.",
                       "Step F must be finished before step E can begin.")

  test("Day7.Problem1") {
    assert(Day7.solve1(testInput) === "CABDFE")
  }
  test("Day7.Problem1_Actual") {
    assert(Day7.solve1() === "CGKMUWXFAIHSYDNLJQTREOPZBV")
  }
  test("Day7.Problem2") {
    assert(Day7.solve2(testInput) === 15)
  }
}
