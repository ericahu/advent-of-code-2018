object Main extends App {
  if (args.isEmpty) {
    println("Season's Greetings! Enter the day and problem numbers to solve (e.g. \"1 2\" for Day 1 Problem 2):")
  }
  val input = if (args.isEmpty) scala.io.StdIn.readLine() else args.mkString(" ")

  input match {
    case "1 1" => println(Day1.solve1())
    case "1 2" => println(Day1.solve2())
    case "2 1" => println(Day2.solve1())
    case "2 2" => println(Day2.solve2())
    case "3 1" => println(Day3.solve1())
    case "3 2" => println(Day3.solve2())
//      case "4 1" => println(Day4.solve1(env))
////        TODO
    case "5 1" => println(Day5.solve1())
    case "5 2" => println(Day5.solve2())
    case "6 1" => println(Day6.solve1())
    case "6 2" => println(Day6.solve2())
    case "7 1" => println(Day7.solve1())
    case "7 2" => println(Day7.solve2())
    case x => println("\n\"" + x + "\" does not exist. Check your input")
  }
}
