object Day7 extends Challenge {
  val inputList = readInput(7)

  def solve1(input: List[String] = inputList): String = {
    val stepPairs = input
      .map ( _.split(" ")
        .toList
        .filter(_.length == 1) )
      .map(_.map(_.head)) // to char list

    val steps = stepPairs.flatten.distinct.sorted
    val prereqs = steps
      .map ( s => (s, stepPairs
        .collect { case x: List[Char] if x(1) == s => x(0) }
    ))
    topologicalSort(List(), prereqs)
  }

  def solve2(input: List[String] = inputList): Int = {
    val stepPairs = input
      .map ( _.split(" ")
        .toList
        .filter(_.length == 1) )
      .map(_.map(_.head)) // to char list

    val steps = stepPairs.flatten.distinct.sorted
    val prereqs = steps
      .map ( s => (s, stepPairs
        .collect { case x: List[Char] if x(1) == s => x(0) }
    ))
    // println(dualTopologicalSort(prereqs, List(), List()))
    0
  }

  def topologicalSort(ans: List[Char], remaining: List[(Char, List[Char])]): String = {

    if (remaining.isEmpty) {
      ans.mkString
    } else {
      val sortedRemaining = remaining
        .sortBy(pair => (pair._2.length, pair._1))

      val newAns = ans :+ sortedRemaining(0)._1
      val newRemaining = sortedRemaining
        .drop(1)
        .map { case (step, prereq) => (step, prereq.filter( !newAns.contains(_) )) }

      topologicalSort(newAns, newRemaining)
    }
  }

  def dualTopologicalSort(
    remaining: List[(Char, List[Char])],
    worker1:   List[(Char, Int)],
    worker2:   List[(Char, Int)]
  ): (String, String) = {

    // Recursion stopping condition
    if (remaining.isEmpty) {
      (worker1.map(_._1).mkString, (worker2.map(_._1).mkString))
      // TODO

    } else {
      val sortedRemaining = remaining
        .sortBy(pair => (pair._2.length, pair._1))

      // Starting worker1
      if (worker1.isEmpty) {
        if (sortedRemaining(0)._2.isEmpty) { // If first task as no prerequisites
          val task = sortedRemaining(0)._1
          worker1 :+ (task, ('A' to 'Z').indexOf(task))
        }
      } else {
        // TODO
      }

      // Starting worker2
      if (worker2.isEmpty) {
        // TODO
      } else {
        // TODO
      }
      ("","")
    }
  }
}
