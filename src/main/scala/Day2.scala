object Day2 extends Challenge {
  val inputList = readInput(2)

  def solve1(input: List[String] = inputList): Int = input.count(has(2)) * input.count(has(3))

  def solve2(input: List[String] = inputList): String = input
    .combinations(2) // get all combination pairs, returns an iterator
    .toList
    .map ( pair =>
      pair(0).zip(pair(1))
        .map { case (charA, charB) =>
          if (charA == charB) charA // include the char if same
          else ""
        }.mkString
    ).maxBy(_.length)

  def has(i: Int) = (s: String) => s
    .groupBy(identity)
    .mapValues(_.size)
    .exists { case (_, value) => value == i }
}