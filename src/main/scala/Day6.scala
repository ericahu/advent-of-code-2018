object Day6 extends Challenge {
  val inputList = readInput(6)

  def solve1(input: List[String] = inputList): Int = {
    val coords = input.map ( _.split(", ").toList )
      .map { case List(x, y) => (x.toInt, y.toInt) }

    val minX = coords.minBy(_._1)._1
    val maxX = coords.maxBy(_._1)._1
    val minY = coords.minBy(_._2)._2
    val maxY = coords.maxBy(_._2)._2

    val grid = genGrid(minX, maxX, minY, maxY)
    val closest = grid.map ( g => singleMin(g, coords) )
      .groupBy(identity)
      .mapValues(_.size)
      .maxBy(_._2)

    closest._2
  }

  def solve2(input: List[String] = inputList, lim: Int = 10000): Int = {
    val coords = input.map ( _.split(", ").toList )
      .map { case List(x, y) => (x.toInt, y.toInt) }

    val minX = coords.minBy(_._1)._1
    val maxX = coords.maxBy(_._1)._1
    val minY = coords.minBy(_._2)._2
    val maxY = coords.maxBy(_._2)._2

    val grid = genGrid(minX, maxX, minY, maxY)
    val sumDist = grid.map ( g => coords
      .map ( manhattanDist(_, g))
      .sum)
      .zip(grid)
      .filter ( _._1 < lim )
    sumDist.length
  }

  def genGrid(minX: Int, maxX: Int, minY: Int, maxY: Int): List[(Int, Int)] = {
    val across = (minX to maxX).toList
    val down = (minY to maxY).toList
    across.flatMap(x => down.map(y => (x, y)))
  }

  def manhattanDist(coordA: (Int, Int), coordB: (Int, Int)): Int = {
    Math.abs(coordA._1 - coordB._1) + Math.abs(coordA._2 - coordB._2)
  }

  def singleMin(targetCoord: (Int, Int), candidates: List[(Int, Int)]): String = {
    val distances = candidates.map ( c => (candidates.indexOf(c).toString, manhattanDist(c, targetCoord) ))
      .sortBy(_._2)
    if (distances(0)._2 == distances(1)._2) "."
    else distances(0)._1
  }
}
