object Day3 extends Challenge {
  val inputList = readInput(3)

  def solve1(input: List[String] = inputList): Int = {

    val inputLine = input.map ( _.split(" ") )
    val starts = inputLine.map ( _(2).dropRight(1).split(",").toList )
    val lengths = inputLine.map ( _(3).split("x").toList )

    val x = starts.zip(lengths)
    val y = x.flatMap { case (s, l) => genCoords(s(0).toInt, s(1).toInt, l(0).toInt, l(1).toInt) }

    val duplicates = y.groupBy(identity)
      .mapValues(_.size)
      .count { case (_, value) => value > 1 }

    duplicates
  }

  def solve2(input: List[String] = inputList): Int = {

    val inputLine = input.map ( _.drop(1).split(" ") )
    val ids = inputLine.map ( _(0) )
    val starts = inputLine.map ( _(2).dropRight(1).split(",") )
    val lengths = inputLine.map ( _(3).split("x") )

    val x = starts.zip(lengths)
    val y = x.map { case (s, l) => genCoords(s(0).toInt, s(1).toInt, l(0).toInt, l(1).toInt) }
    val flatY = y.flatten
    val z = ids.zip(y)

    val duplicates = flatY.groupBy(identity)
      .mapValues(_.size)
      .filter { case (_, value) => value > 1 }

    val notDuplicated = z.filter {
      case (_, coordsList) => coordsList
        .forall ( coord => !duplicates
          .contains(coord))
    }
    notDuplicated(0)._1.toInt
  }

  def genCoords(left: Int, top: Int, width: Int, height: Int): List[(Int, Int)] = {
    val across = (left until left + width).toList
    val down = (top until top + height).toList
    across.flatMap(a => down.map(b => (a, b)))
  }
}
