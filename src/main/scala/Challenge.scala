import scala.io._
import scala.util.{Try, Success, Failure}

case class Challenge() {

  def readInput(dayNum: Int, failCount: Int = 0): List[String] = {
    if (failCount >= 3) {
      List()
    }
    val path = System.getProperty("user.dir") + "/src/input/Day" + dayNum + ".in"
    if (dayNum == 1) println(path)
    val input = Try ( Source.fromFile(path)
      .getLines
      .toList )
    input match {
      case Success(i) => i
      case Failure(e) =>
        println("INPUT FILE NOT FOUND. " + e.getMessage)
        throw new Exception("INPUT FILE NOT FOUND")
    }
  }

  // def readInput(dayNum: Int): List[String] = {
  //   val path = System.getProperty("user.dir") + "/src/input/" + "Day" + dayNum + ".in"
  //
  //   Try ( Source.fromFile(path)
  //     .getLines
  //     .toList ) {
  //       case e => throw FileNotFoundException("OOPS")
  //     }
  // }
}


// import scala.util.{Try, Success, Failure}
//
// def divide: Try[Int] = {
//   val dividend = Try(Console.readLine("Enter an Int that you'd like to divide:\n").toInt)
//   val divisor = Try(Console.readLine("Enter an Int that you'd like to divide by:\n").toInt)
//   val problem = dividend.flatMap(x => divisor.map(y => x/y))
//   problem match {
//     case Success(v) =>
//       println("Result of " + dividend.get + "/"+ divisor.get +" is: " + v)
//       Success(v)
//     case Failure(e) =>
//       println("You must've divided by zero or entered something that's not an Int. Try again!")
//       println("Info from the exception: " + e.getMessage)
//       divide
//   }
// }
