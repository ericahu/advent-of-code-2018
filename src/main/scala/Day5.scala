object Day5 extends Challenge {
  val inputString = readInput(5).flatten.mkString

  def solve1(input: String = inputString): Int = replace(input, pattern).length
  def solve2(input: String = inputString): Int = improvedMin(input)

  def replace(input: String, pattern: String): String = {
    if (input.isEmpty) {
      ""
    } else {
      val newInput = input.replaceAll(pattern, "")
      if (newInput.length == input.length) {
        newInput
      } else {
        replace(newInput, pattern)
      }
    }
  }

  def improvedMin(input: String): Int = {
    ('a' to 'z').toList
      .map ( c => input.replaceAll(c.toString + "|" + c.toUpper.toString, ""))
      .map(replace(_, pattern).length).min
  }

  val pattern = (('a' to 'z').zip('A' to 'Z') ++ ('A' to 'Z').zip('a' to 'z'))
    .toList
    .map { case (a, b) => s"$a$b" }
    .mkString("|")
}
