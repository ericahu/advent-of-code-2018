object Day1 extends Challenge {
  val inputList = readInput(1)
  println(inputList)

  def solve1(input: List[String] = inputList): Int = parseInput(input).sum

  def solve2(input: List[String] = inputList): Int =
    check(input = parseInput(input), seen = List(0), current = 0)

  def parseInput(input: List[String]): List[Int] = {
    input.map ( num =>
      if (num(0) != '-' && num(0) != '+') num.drop(1).toInt
      else num.toInt )
  }

  def check(input: List[Int], seen: List[Int], current: Int): Int = {
    val newCurrent = current + input(0)
    if (seen.contains(newCurrent)) newCurrent
    else check((input :+ input(0)).drop(1), seen :+ newCurrent, newCurrent)
  }
}
