name := "AdventOfCode2018"

version := "0.1"

scalaVersion := "2.12.8"

libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.1.0"

libraryDependencies += "junit" % "junit" % "4.10" % Test

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"